import { Banner } from 'components/Banner'
import ImageLocation from 'components/ImageLocation'
import ProductCategory from 'components/ProductCategory'
import React from 'react'

const Homepage = () => {
    return (
        <>
            <Banner />
            <ProductCategory />
            <ImageLocation />
        </>
    )
}

export default Homepage