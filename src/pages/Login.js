import Axios from 'axios';
import React, { useState } from 'react'


const Login = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [isLoading, setIsLoading] = useState(false);


  const handleSubmit = (event) => {
    event.preventDefault();
    setIsLoading(true);

    Axios
      .post("https://api-tdp-2022.vercel.app/api/login", {
        username,
        password,
      })
      .then(function (response) {
        const { data } = response;
        localStorage.setItem("token", data.data.access_token);
        localStorage.setItem("user_info", username);
        localStorage.setItem("isLoggedIn", true);
        setIsLoading(false);

        window.location.replace("/");
      })
      .catch(function (error) {
        alert(error?.response?.data?.message);
        setIsLoading(false);
      });
  };

  return (
    <div className="content padding" style={{ maxWidth: '1564px' }}>
      <div className="container padding-32" id="contact" >
        <h3 className="border-bottom border-light-grey padding-16">Login</h3>
        <p>Lets get in touch and talk about your next project.</p>
        <form onSubmit={handleSubmit}>
          <input autoComplete='off' onChange={(e) => setUsername(e.target.value)} value={username} className="input border" type="text" placeholder="Username" required name="username" />
          <input onChange={(e) => setPassword(e.target.value)} value={password} className="input section border" type="password" placeholder="Password" required name="Password" />
          <button className="button black section" type="submit" disabled={isLoading}>
            <i className="fa fa-paper-plane" />
            {isLoading ? "Loading..." : "Login"}
          </button>
        </form>
      </div >
    </div>
  )
}

export default Login


