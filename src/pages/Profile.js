import Axios from 'axios';
import React, { useEffect, useState } from 'react'

const Profile = () => {
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [phone, setPhone] = useState("");
    const [password, setPassword] = useState("");
    const [repassword, setRepassword] = useState("");
    const [loading, setLoading] = useState(false);
    const username = localStorage.getItem("user_info")
    let dataValidation = true;
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);

    useEffect(() => {
        setLoading(true)
        Axios.get(`https://api-tdp-2022.vercel.app/api/profile/${username}`)
            .then(function (response) {
                const { data } = response;

                setName(data.data.name);
                setEmail(data.data.email);
                setPhone(data.data.phoneNumber);
            })
            .catch(function (error) {
                console.log("error", error)
            }).then(function () {
                setLoading(false)
            });
    }, [])

    const validationName = () => {

        if (!name) {
            document.getElementById("name").style.display = 'block'
            dataValidation = false;
        }
        else {
            document.getElementById("name").style.display = 'none'

        }
    }

    const validationEmail = () => {

        if (!email) {
            document.getElementById("email").innerText = 'Email is Required'
            document.getElementById("email").style.display = 'block'
            dataValidation = false;
        }
        else {
            if (!email.match(pattern)) {
                document.getElementById("email").innerText = 'Email Invalid'
                document.getElementById("email").style.display = 'block'
                dataValidation = false;
            }
            else {
                document.getElementById("email").style.display = 'none'
            }
        }
    }

    const validationPhone = () => {

        if (!phone) {
            document.getElementById("phone").innerText = 'Phone Number is Required'
            document.getElementById("phone").style.display = 'block'
            dataValidation = false;
        }
        else {
            if (!phone.match(/^[0-9]{12}$/)) {
                document.getElementById("phone").innerText = 'Phone Number Invalid'
                document.getElementById("phone").style.display = 'block'
                dataValidation = false;
            }
            else {
                document.getElementById("phone").style.display = 'none'
            }
        }
    }

    const validationPassword = () => {

        if (!password) {
            document.getElementById("password").style.display = 'block'
            dataValidation = false;
        }
        else {
            document.getElementById("password").style.display = 'none'
        }
    }

    const validationRepassword = () => {

        if (!repassword) {
            document.getElementById("repassword").innerText = 'Retype Password is Required'
            document.getElementById("repassword").style.display = 'block'
            dataValidation = false;
        }
        else {
            if (repassword !== password) {
                document.getElementById("repassword").innerText = 'Retype Password Invalid'
                document.getElementById("repassword").style.display = 'block'
                dataValidation = false;
            }
            else {
                document.getElementById("repassword").style.display = 'none'
            }
        }
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        validationName();
        validationEmail();
        validationPhone();
        validationPassword();
        validationRepassword();
        console.log('valid', dataValidation)


        if (dataValidation) {
            Axios
                .put(`https://api-tdp-2022.vercel.app/api/profile/${username}`, {
                    name,
                    email,
                    phoneNumber: phone,
                    password,
                })
                .then(function (response) {
                    const { data } = response;
                    localStorage.setItem("token", data.data.access_token);
                    localStorage.setItem("isLoggedIn", true);
                    alert('Data has been add Successfully!');
                    window.location.replace("/");
                })
                .catch(function (error) {
                    alert(error?.response?.data?.message);
                });
        }
    }



    return (
        <div className="content padding" style={{ maxWidth: '1564px' }}>
            <div className="container padding-32" id="contact" >
                <h3 className="border-bottom border-light-grey padding-16">My Profile</h3>
                <p>Lets get in touch and talk about your next project.</p>
                <form onSubmit={handleSubmit}>
                    <input onChange={(e) => setName(e.target.value)} defaultValue={name} className="input section border" type="text" placeholder="Name" name="name" />
                    <div id='name' style={{ color: '#EF144A', display: 'none' }}>Name is required</div>
                    <input onChange={(e) => setEmail(e.target.value)} defaultValue={email} className="input section border" type="text" placeholder="Email" name="email" />
                    <div id='email' style={{ color: '#EF144A', display: 'none' }}>Email is required</div>
                    <input onChange={(e) => setPhone(e.target.value)} defaultValue={phone} className="input section border" type="text" placeholder="Phone Number" name="phoneNumber" maxLength={12} />
                    <div id='phone' style={{ color: '#EF144A', display: 'none' }}>Phone Number is required</div>
                    <input onChange={(e) => setPassword(e.target.value)} className="input section border" type="password" placeholder="Password" name="password" />
                    <div id='password' style={{ color: '#EF144A', display: 'none' }}>Password is required</div>
                    <input onChange={(e) => setRepassword(e.target.value)} className="input section border" type="password" placeholder="Retype Password" name="retypePassword" />
                    <div id='repassword' style={{ color: '#EF144A', display: 'none' }}>Retype Password is required</div>
                    <button className="button black section" type="submit">
                        <i className="fa fa-paper-plane" /> Update
                    </button>
                </form>
            </div>
        </div>
    )
}

export default Profile