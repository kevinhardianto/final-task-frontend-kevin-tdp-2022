import React, { useEffect, useState } from 'react'
import Axios from 'axios'
import { useSearchParams } from "react-router-dom";

const ListProduct = () => {
    const [list, setList] = useState([]);
    const [loading, setLoading] = useState();
    const [searchParams] = useSearchParams();
    const categoryId = searchParams.get("category");

    useEffect(() => {
        setLoading(true);

        let urlLink = categoryId
            ? `https://api-tdp-2022.vercel.app/api/products?category=${categoryId}`
            : "https://api-tdp-2022.vercel.app/api/products";

        Axios.get(urlLink)
            .then(function (response) {
                const { data } = response;

                setList(data.data.productList);

            })
            .catch(function (error) {
                console.log("error", error)
            }).then(function () {
                setLoading(false)
            });

    }, [])

    const rupiah = (money) => {
        return new Intl.NumberFormat('id-ID',
            { style: 'currency', currency: 'IDR', minimumFractionDigits: 0 }
        ).format(money);
    }

    return (

        <div className="content padding" style={{ maxWidth: '1564px' }}>
            <div className="container padding-32" id="about">
                {
                    categoryId ?
                        <div className="container padding-32" id="projects">
                            <h3 className="border-bottom border-light-grey padding-16">Products Category</h3>
                        </div>
                        : <h3 className="border-bottom border-light-grey padding-16">
                            All Product
                        </h3>
                }

            </div>
            {
                loading ? (
                    <div className="row-padding padding-large" style={{ fontSize: '16px', fontWeight: '800', textAlign: 'center' }}>
                        Loading . . .
                    </div>
                ) :
                    <div className="row-padding rm-before-after" style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'center' }}>
                        {
                            list?.map(product => {
                                if (product.discount !== 0) {
                                    return (
                                        <div className="product-card">
                                            <div id='disc' className="badge">Discount</div>
                                            <div className="product-tumb">
                                                <img src={product.image} alt="product1" />
                                            </div>
                                            <div className="product-details">
                                                <span className="product-catagory">{product.category}</span>
                                                <h4>
                                                    <a href={`/shop/${product.id}`}>{product.title}</a>
                                                </h4>
                                                <p>{product.description}</p>
                                                <div className="product-bottom-details">
                                                    <div className="product-price">{rupiah(product.price)}</div>
                                                    <div className="product-links">
                                                        <a href={`/shop/${product.id}`}>View Detail<i className="fa fa-heart"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    )
                                }
                                return (
                                    <div className="product-card">
                                        <div className="product-tumb">
                                            <img src={product.image} alt="product1" />
                                        </div>
                                        <div className="product-details">
                                            <span className="product-catagory">{product.category}</span>
                                            <h4>
                                                <a href={`/shop/${product.id}`}>{product.title}</a>
                                            </h4>
                                            <p>{product.description}</p>
                                            <div className="product-bottom-details">
                                                <div className="product-price">{rupiah(product.price)}</div>
                                                <div className="product-links">
                                                    <a href={`/shop/${product.id}`}>View Detail<i className="fa fa-heart"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </div>
            }
        </div>
    )
}

export default ListProduct