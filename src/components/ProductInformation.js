import React, { useEffect, useState } from 'react'
import Axios from 'axios'
import { useParams } from 'react-router-dom'

const ProductInformation = () => {
    const [product, setProduct] = useState([]);
    const [afterDisc, setafterDisc] = useState(0);
    const [beforeDisc, setbeforeDisc] = useState(0);
    const [loading, setLoading] = useState()
    const isLoggedIn = localStorage.getItem("isLoggedIn");
    let params = useParams();

    useEffect(() => {
        setLoading(true)
        Axios.get(`https://api-tdp-2022.vercel.app/api/products/${params?.productId}`)
            .then(function (response) {
                const { data } = response;

                setProduct(data.data);
            })
            .catch(function (error) {
                console.log("error", error)
            }).then(function () {
                setLoading(false)
            });
    }, [])

    useEffect(() => {
        document.getElementById("add").disabled = true;
        if (product.discount !== 0) {
            document.getElementById("disc").style.opacity = '100%';
        }
        else {
            document.getElementById("disc").style.opacity = '0%';
        }
    }, [product.discount])

    useEffect(() => {
        if (product.stock !== 0) {
            document.getElementById("empty").style.opacity = '0%';
        }
        else {
            document.getElementById("empty").style.opacity = '100%';
            document.getElementById("add").style.visibility = 'hidden';
            document.getElementsByName("total")[0].readOnly = 'true';
        }
    }, [product.stock])

    const rupiah = (money) => {
        return new Intl.NumberFormat('id-ID',
            { style: 'currency', currency: 'IDR', minimumFractionDigits: 0 }
        ).format(money);
    }

    const handleChangeNumber = (e) => {
        const number = e.target.value;

        if (parseInt(number) !== 0 && number !== "") {
            document.getElementById("add").disabled = false;
        }
        else {
            document.getElementById("add").disabled = true;
        }

        setafterDisc((number * product.price) - (((number * product.price) * product.discount) / 100));
        setbeforeDisc(number * product.price)
    }

    function handleClick() {
        if (!isLoggedIn) {
            window.location.replace("/login")
        }
        else {
            alert('Product added to cart successfully');
            window.location.replace("/");
        }
    }

    return (
        <div className="content padding" style={{ maxWidth: '1564px' }}>
            <div className="container padding-32" id="about">
                <h3 className="border-bottom border-light-grey padding-16">Product Information</h3>
            </div>
            <div className="row-padding card card-shadow padding-large" style={{ marginTop: '50px' }}>
                <div className="col l3 m6 margin-bottom">
                    <div className="product-tumb">
                        <img src={product.image} alt="Product 1" />
                    </div>
                </div>
                <div className="col m6 margin-bottom">
                    <h3>{product.title}</h3>
                    <div style={{ marginBottom: '32px' }}>
                        <span>Category : <strong>{product.category}</strong></span>
                        <span style={{ marginLeft: '30px' }}>Review : <strong>{product.rate}</strong></span>
                        <span style={{ marginLeft: '30px' }}>Stock : <strong>{product.stock}</strong></span>
                        <span style={{ marginLeft: '30px' }}>Discount : <strong>{product.discount}%</strong></span>
                    </div>
                    <div style={{ fontSize: '2rem', lineHeight: '34px', fontWeight: '800', marginBottom: '32px' }}>
                        {rupiah(product.price)}
                    </div>
                    <div style={{ marginBottom: '32px' }}>
                        {product.desciption}
                    </div>
                    <div style={{ marginBottom: '32px' }}>
                        <div><strong>Quantity : </strong></div>
                        <input type="number" onChange={handleChangeNumber} defaultValue='0' className="input section border" min={0} max={product.stock} name="total" placeholder='Quantity' />
                    </div>
                    <div style={{ marginBottom: '32px', fontSize: '2rem', fontWeight: 800 }}>
                        Sub Total : {rupiah(afterDisc)}
                        <span id='disc' style={{ marginLeft: '30px', fontSize: '18px', textDecoration: 'line-through' }}><strong>{rupiah(beforeDisc)}</strong></span>
                    </div>

                    <button onClick={handleClick} id='add' className='button light-grey block' >Add to cart</button>
                    <button id='empty' className='button light-grey block' disabled={true}>Empty Stock</button>
                </div>
            </div>

        </div>
    )
}

export default ProductInformation