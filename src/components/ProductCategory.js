import React, { useEffect, useState } from 'react'
import Axios from 'axios'

const ProductCategory = () => {
    const [product, setProduct] = useState([])

    useEffect(() => {
        Axios.get(`https://api-tdp-2022.vercel.app/api/categories`)
            .then(function (response) {
                const { data } = response;

                setProduct(data.data);
            })
            .catch(function (error) {
                console.log("error", error)
            });
    }, [])

    return (
        <div>
            <div className="content padding" style={{ maxWidth: '1564px' }}>
                <div className="container padding-32" id="projects">
                    <h3 className="border-bottom border-light-grey padding-16">Products Category</h3>
                </div>
            </div>
            {
                <div className="row-padding">
                    {
                        product?.map(product => {
                            return (
                                <div className="col l3 m6 margin-bottom">
                                    <a href={`/shop?category=${product.id}`}>
                                        <div className="display-container" style={{ boxShadow: '0 2px 7px #dfdfdf', display: 'flex', alignItems: 'center', justifyContent: 'center', height: '300px', padding: '80px' }}>
                                            <div className="display-topleft black padding">{product.name}</div>
                                            <img src={product.image} alt="House" style={{ maxWidth: '100%', minHeight: '100%' }} />
                                        </div>
                                    </a>
                                </div>
                            )
                        })
                    }
                </div>
            }
        </div>
    )
};

export default ProductCategory
