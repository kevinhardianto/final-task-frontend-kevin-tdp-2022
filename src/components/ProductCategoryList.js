import React, { useEffect, useState } from 'react'
import Axios from 'axios'
import { useParams } from 'react-router-dom'

const ProductCategoryList = () => {
    const [list, setList] = useState([]);
    const [loading, setLoading] = useState()
    let params = useParams();

    useEffect(() => {
        setLoading(true)
        Axios.get(`https://api-tdp-2022.vercel.app/api/products?category=${params?.categoryId}`)
            .then(function (response) {
                const { data } = response;

                setList(data.data.productList);
                
            })
            .catch(function (error) {
                console.log("error", error)
            }).then(function () {
                setLoading(false)
            });
    }, [])

    return (
        <div>
            <div className="content padding" style={{ maxWidth: '1564px' }}>
                <div className="container padding-32" id="projects">
                    <h3 className="border-bottom border-light-grey padding-16">Products Category {list.category}</h3>
                </div>
            </div>
            {
                loading ? (
                    <div className="row-padding padding-large" style={{ fontSize: '16px', fontWeight: '800', textAlign: 'center' }}>
                        Loading . . .
                    </div>
                ) :
                    <div className="row-padding rm-before-after" style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'center' }}>
                        {
                            list?.map(product => {
                                if (product.discount !== 0) {
                                    return (
                                        <div className="product-card">
                                            <div id='disc' className="badge">Discount</div>
                                            <div className="product-tumb">
                                                <img src={product.image} alt="product1" />
                                            </div>
                                            <div className="product-details">
                                                <span className="product-catagory">{product.category}</span>
                                                <h4>
                                                    <a href={`/shop/${product.id}`}>{product.title}</a>
                                                </h4>
                                                <p>{product.description}</p>
                                                <div className="product-bottom-details">
                                                    <div className="product-price">Rp. {product.price}</div>
                                                    <div className="product-links">
                                                        <a href={`/shop/${product.id}`}>View Detail<i className="fa fa-heart"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    )
                                }
                                return (
                                    <div className="product-card">
                                        <div className="product-tumb">
                                            <img src={product.image} alt="product1" />
                                        </div>
                                        <div className="product-details">
                                            <span className="product-catagory">{product.category}</span>
                                            <h4>
                                                <a href={`/shop/${product.id}`}>{product.title}</a>
                                            </h4>
                                            <p>{product.description}</p>
                                            <div className="product-bottom-details">
                                                <div className="product-price">Rp. {product.price}</div>
                                                <div className="product-links">
                                                    <a href={`/shop/${product.id}`}>View Detail<i className="fa fa-heart"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </div>
            }
        </div>
    )
}

export default ProductCategoryList