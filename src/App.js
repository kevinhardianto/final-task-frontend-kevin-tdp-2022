import { Routes, Route, Navigate } from 'react-router-dom';
import 'assets/css/style.css';
import 'assets/css/custom.css';
import { Menu } from 'components/Menu';
import Homepage from 'pages/Homepage';
import Footer from 'components/Footer';
import Shoppage from 'pages/Shoppage';
import ShopDetail from 'pages/ShopDetail';
import Profile from 'pages/Profile';
import Login from 'pages/Login';

const RequireAuth = ({ children }) => {
    const isLoggedIn = window.localStorage.getItem("isLoggedIn");
    return isLoggedIn ? children : <Navigate to="/login" />
};

const App = () => {
    return (
        <>
            <Menu />
            <Routes>
                <Route path='/' element={<Homepage />} />
                <Route path='/shop' element={<Shoppage />} />
                <Route path='/shop/:productId' element={<ShopDetail />} />
                <Route path='/login' element={<Login />} />
                <Route path='/profile' element={
                    <RequireAuth>
                        <Profile />
                    </RequireAuth>
                } />
            </Routes>
            <Footer />S
        </>
    );
}

export default App;
